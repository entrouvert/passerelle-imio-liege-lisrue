from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImioLiegeLisrue',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField(unique=True)),
                ('description', models.TextField()),
                (
                    'service_url',
                    models.CharField(
                        help_text='SIG Web Service URL (ex: https://e-services.liege.be:8443/)',
                        max_length=128,
                        verbose_name='Service URL',
                    ),
                ),
                (
                    'verify_cert',
                    models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
                ),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Liege Lisrue Service',
            },
            bases=(models.Model,),
        ),
    ]
