from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_liege_lisrue', '0005_auto_20170921_1213'),
    ]

    operations = [
        migrations.AddField(
            model_name='imioliegelisrue',
            name='street_with_postal_code',
            field=models.BooleanField(default=True, verbose_name='Return street with postal code'),
        ),
        migrations.AlterField(
            model_name='imioliegelisrue',
            name='description',
            field=models.TextField(verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='imioliegelisrue',
            name='title',
            field=models.CharField(max_length=50, verbose_name='Title'),
        ),
    ]
