from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_liege_lisrue', '0006_auto_20180301_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imioliegelisrue',
            name='street_with_postal_code',
            field=models.BooleanField(default=False, verbose_name='Return street with postal code'),
        ),
    ]
