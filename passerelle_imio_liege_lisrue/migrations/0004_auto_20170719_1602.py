from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_liege_lisrue', '0003_imioliegelisrue_include_all_of_belgium'),
    ]

    operations = [
        migrations.AddField(
            model_name='imioliegelisrue',
            name='profile',
            field=models.CharField(
                default=b'Liege', max_length=100, choices=[(b'LIEGE', b'Liege'), (b'NAMUR', b'Namur')]
            ),
        ),
        migrations.AlterField(
            model_name='imioliegelisrue',
            name='log_level',
            field=models.CharField(
                default=b'INFO',
                max_length=10,
                verbose_name='Log Level',
                choices=[
                    (b'NOTSET', b'NOTSET'),
                    (b'DEBUG', b'DEBUG'),
                    (b'INFO', b'INFO'),
                    (b'WARNING', b'WARNING'),
                    (b'ERROR', b'ERROR'),
                    (b'CRITICAL', b'CRITICAL'),
                    (b'FATAL', b'FATAL'),
                ],
            ),
        ),
    ]
