from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_liege_lisrue', '0002_imioliegelisrue_log_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='imioliegelisrue',
            name='include_all_of_belgium',
            field=models.BooleanField(default=True, verbose_name='Include all of Belgium'),
        ),
    ]
