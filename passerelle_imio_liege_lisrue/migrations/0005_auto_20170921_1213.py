from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_liege_lisrue', '0004_auto_20170719_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imioliegelisrue',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
